import React, { Component } from 'react';
import {
	MenuItem,
	TextField,
	Typography,
	Paper,
	Grid,
	Icon,
	IconButton, 
	FormControl,
	Input,
	InputLabel,
	InputAdornment
} from '@material-ui/core';
import * as FontAwesome from 'react-icons/lib/fa';

import {
  SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguage,
	SelectLanguageNative,
	UploadButton,
	DateNow
} from '../../utils';

class Modify extends Component {
	state = {
		open: false,
		form: {
			tools: {}
		},
		send: {
			tools: {}
		},
		showpassword: false
	}
	constructor(props){
		super(props)

		this.firestore = props.firebase.firestore().collection('users');
		this.storage   = props.firebase.storage();
		this.onSubmit  = this.onSubmit.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
		this.props.handleOnClick(this.handleOnClick)
	}
	componentWillMount(){
		if(
			this.props.location.state !== undefined	 
				&&
			'id' in this.props.location.state
				&&
			this.props.location.state.id !== undefined
		){
			this.setState({ uid: this.props.location.state.id }, () => this.handleDateInfo())
		}
	}
	componentWillUpdate(prevProps ,prevState){
		if(
			prevProps.location.state !== undefined 
				&& 
			'id' in prevProps.location.state 
				&&
			prevProps.location.state.id !== undefined
				&& 
			prevState.uid !== prevProps.location.state.id
		){
			this.setState(
				{ 
					uid: prevProps.location.state.id,
					send: { tools: {} } 
				}, () => this.handleDateInfo())
		}
	}
	handleDateInfo = () => {
		this.firestore
			.doc(this.state.uid)
			.onSnapshot(snapshot => {
				if(snapshot.exists){
					this.setState({
						form: {
							...snapshot.data(),
							id: snapshot.id
						}
					}, () => console.log(this.state.form))
				}else console.log('Not Found Users')
			}, error => console.log(error))
	}
	handleChange = name => event => {
		if(this.state.form[name] === this.state.send[name])
			return;
		this.setState({
			send: {
				...this.state.send,
				[name]: event.target.value
			}
		})
	}
	handleChangeTools = name => event => {
		if([name].indexOf(this.state.form.tools) !== -1 && this.state.form.tools[name] === this.state.send.tools[name])
			return;
		this.setState({
			send: {
				...this.state.send,
				tools: {
					...this.state.send.tools,
					[name]: event.target.value
				}
			}	
		})
	}
	UploadFile = (userId, mes) => {
		let type = this.state.send.file.type.split('/')[1];
		const storage  = this.storage.ref(),
					route    = `users/${userId}/thumbnails/${userId}.${type}`,
					photo 	 = storage.child(route),
					task     = photo.put(this.state.send.file); 

		task.on('state_changed', 
			snapshot => {},
			err => console.log(`Error: ${err.code} ${err.message}`),
			() => {
				console.log(task.snapshot.downloadURL, route)
				this.firestore
						.doc(userId)
						.update({ photo : task.snapshot.downloadURL, photoRef: route })
						.then(response => {
							setTimeout(() => {
								this.props.handleSnackbar({type: 'custom', message: mes})
								this.setState({ 
									form: {
										tools: {}
									} 
								})
								this.props.handleOnLoading('hide')
								this.props.handleChange(null, 0)
							}, 2500)
							this.props.handleOnLoading('done')
						})
						.catch(error => {
							console.log(error)
							this.props.handleOnLoading('hide')
							if(error.errorInfo)
								return this.props.handleSnackbar({ type: 'storage', error:  error.errorInfo, on: true})
							return this.props.handleSnackbar({ type: 'custom', message: error, on: true })
						}) 
			}
		)
	}
	onSubmit = event => {
		event.preventDefault();
		this.props.handleOnLoading('show')
		console.log(this.state.send)
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/users/'+this.state.uid,
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'PUT',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token,
					...this.state.send
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			if(response.success === true){
				if(this.state.send.file)
					this.UploadFile(response.id, response.mes) 
				else
					this.props.handleSnackbar({type: 'custom', message: response.mes})
					setTimeout(() => {
						this.props.handleOnLoading('hide')
						this.props.handleChange(null, 0)
					}, 2500) 
			}else {
				this.props.handleOnLoading('hide')
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleOnLoading('hide')
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleOnClick = () => {
		document.getElementById('modify/submit').click()
	}
	render() {
		const { id, value } = this.props;
		const { showpassword } = this.state;

		return (
			<Paper style={{padding: 15}}>
				<form 
					id="modify"
					onSubmit={this.onSubmit} 
					style={{ display: value === id ? 'block' : 'none' }}
				>
					<Typography variant="title" gutterBottom>Editar usuario</Typography>
					<Grid container>
					<Grid
							item 
							xs={12}
							sm={12}
							md={12}
							lg={12}
							xl={12}
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							<Typography variant="body2" style={{marginTop: 10}}>Datos del usuario</Typography>
						</Grid>
						<Grid 
							item 
							xs={12} 
							sm={12} 
							md={6} 
							lg={6} 
							xl={6} 
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							<TextField
								id='modify/username'
								label="Nombre de usuario"
								margin="normal"
								fullWidth
								// required
								// error={this.state.error.username}
								type='text'
								value={
									this.state.send.hasOwnProperty('username')
										?
									this.state.send.username
										:
									this.state.form.username 
										? 
									this.state.form.username 
										: 
									''
								}
								onChange={this.handleChange('username')}
							/>
							<TextField
								id="modify/mail"
								label="Correo electronico"
								margin="normal"
								fullWidth
								// required
								// error={this.state.error.email}
								value={
									this.state.send.hasOwnProperty('email')
										?
									this.state.send.email
										:
									this.state.form.email 
										? 
									this.state.form.email 
										: 
									''
								}
								type='email'
								onChange={this.handleChange('email')}
							/>
							<FormControl
								margin="normal"
								fullWidth
							>
								<InputLabel htmlFor="password">Contraseña</InputLabel> 	
								<Input
									id="modify/password"
									label="Contraseña"
									// required
									type={ showpassword ? 'text' : 'password' }
									onChange={event => 
										this.setState({ 
											send: {
												...this.state.send, 
												password: event.target.value
											} 
										})
									}
									endAdornment={
										<InputAdornment position="end">
											<IconButton
												aria-label="Toggle password visibility"
												onClick={
													event => this.setState((prevState, props) => {
														return {showpassword: !prevState.showpassword}
													}
												)}
												onMouseDown={event => event.preventDefault()}
											>
												<Icon>{ showpassword ? 'visibility' : 'visibility_off' }</Icon>
											</IconButton>
										</InputAdornment>
									}
								/>
							</FormControl>
							<TextField
								id="modify/priv"
								select
								label="Privilegio"
								// required
								value={
									this.state.send.hasOwnProperty('priv')
										?
									this.state.send.priv
										:
									this.state.form.priv 
										? 
									this.state.form.priv 
										: 
									''
								}
								onChange={this.handleChange('priv')}
								fullWidth
								margin="normal"
							>
								<MenuItem key={1} value={1}>
									Administrador
								</MenuItem>
								<MenuItem key={2} value={2}>
									Supervisor
								</MenuItem>
								<MenuItem key={3} value={3}>
									Usuario
								</MenuItem>
							</TextField>
							{
								this.state.form.priv !== 1 && this.state.form.priv !== 2
									?
								<SelectCurrentMoney 
									id="modify/money" 
									label="Moneda"
									value={
										this.state.send.hasOwnProperty('money')
											?
										this.state.send.money
											:
										this.state.form.money 
											? 
										this.state.form.money 
											: 
										''
									}  
									onChange={this.handleChange('money')}
									firebase={this.props.firebase}
								/>
									:
								null
							}
							<TextField
								id="modify/watch"
								select
								label="Tipo hora"
								// required
								value={
									this.state.send.hasOwnProperty('watch')
										?
									this.state.send.watch
										:
									this.state.form.watch 
										? 
									this.state.form.watch 
										: 
									''
								}
								onChange={this.handleChange('watch')}
								fullWidth
								margin="normal"
							>
								<MenuItem key={1} value={1}>
									24Hrs
								</MenuItem>
								<MenuItem key={2} value={2}>
									12Hrs
								</MenuItem>
							</TextField>
							<Grid container>
								<Grid
									item
									xs={12}	
									ms={12}	
									md={8}	
									lg={8}	
									xl={8}	
								>
									<SelectTimeZone 
										id="modify/timezone"
										label="Zona horaria"
										value={
											this.state.send.hasOwnProperty('timezone')
												?
											this.state.send.timezone
												:
											this.state.form.timezone 
												? 
											this.state.form.timezone 
												: 
											''
										}
										onChange={this.handleChange('timezone')}
									/>
								</Grid>
								<Grid
									item
									xs={12}	
									ms={12}	
									md={4}	
									lg={4}	
									xl={4}	
								>
									<DateNow
										timezone={this.state.form.timezone}
										watch={this.state.form.watch}
									/>
								</Grid>
							</Grid>
							
						</Grid>
						<Grid 
							item 
							xs={12} 
							sm={12} 
							md={6} 
							lg={6} 
							xl={6} 
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							<TextField
								id="modify/fullname"
								label="Nombre completo"
								margin="normal"
								fullWidth
								// required
								// error={false}
								type='text'
								value={
									this.state.send.hasOwnProperty('fullname')
										?
									this.state.send.fullname
										:
									this.state.form.fullname 
										? 
									this.state.form.fullname 
										: 
									''
								}
								inputProps={{
									pattern: '[A-Za-z ]{1,}',
									title: 'Solo se aceptan letras'
								}}
								onChange={this.handleChange('fullname')}
							/>
							<TextField
								id="modify/mobile"
								label="Telefono movil"
								placeholder="+581234567890"
								margin="normal"
								fullWidth
								// required
								// error={false}
								type='tel'
								inputProps={{
									pattern: '[+][0-9]{12}',
									title: 'Solo se aceptan letras'
								}}
								value={
									this.state.send.hasOwnProperty('mobile')
										?
									this.state.send.mobile
										:
									this.state.form.mobile 
										? 
									this.state.form.mobile 
										: 
									''
								}
								onChange={this.handleChange('mobile')}
							/>
							<TextField
								id="modify/phone"
								label="Telefono casa"
								placeholder="+581234567890"
								margin="normal"
								fullWidth
								// required
								// error={false}
								type='tel'
								inputProps={{
									pattern: '[+][0-9]{12}',
									title: 'Solo se aceptan letras'
								}}
								value={
									this.state.send.hasOwnProperty('phone')
										?
									this.state.send.phone
										:
									this.state.form.phone 
										? 
									this.state.form.phone 
										: 
									''
								}
								onChange={this.handleChange('phone')}
							/>
							<TextField
								id="modify/gender"
								select
								label="Genero"
								// required
								value={
									this.state.send.hasOwnProperty('gender')
										?
									this.state.send.gender
										:
									this.state.form.gender 
										? 
									this.state.form.gender 
										: 
									''
								}
								onChange={this.handleChange('gender')}
								fullWidth
								margin="normal"
							>
								<MenuItem key={'M'} value={'M'}>
									Masculino
								</MenuItem>
								<MenuItem key={'F'} value={'F'}>
									Femenino
								</MenuItem>
							</TextField>
							<TextField
								id="modify/birthdate"
								label="Fecha de cumpleaños"
								margin="normal"
								fullWidth
								// required
								type='date'
								InputLabelProps={{
									shrink: true,
								}}
								value={
									this.state.send.hasOwnProperty('birthdate')
										?
									this.state.send.birthdate
										:
									this.state.form.birthdate 
										? 
									this.state.form.birthdate 
										: 
									''
								}
								onChange={this.handleChange('birthdate')}
							/>
							<TextField
								id="modify/address"
								label="Direccion"
								multiline
								fullWidth
								margin="normal"
								value={
									this.state.send.hasOwnProperty('address')
										?
									this.state.send.address
										:
									this.state.form.address 
										? 
									this.state.form.address 
										: 
									''
								}
								onChange={this.handleChange('address')}
							/>
							{
								this.state.form.priv !== 1 && this.state.form.priv !== 2
									?
								<TextField
									id="modify/company"
									label="Compañia"
									margin="normal"
									type="text"
									fullWidth
									value={
										this.state.send.hasOwnProperty('company')
											?
										this.state.send.company
											:
										this.state.form.company 
											? 
										this.state.form.company 
											: 
										''
									}
									onChange={this.handleChange('company')}
								/>
									:
								null
							}
						</Grid>
						<Grid
							item 
							xs={12}
							sm={12}
							md={12}
							lg={12}
							xl={12}
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							{
								this.state.form.priv !== 1 && this.state.form.priv !== 2
									?
								<TextField
									id="modify/website"
									label="Sitio web"
									margin="normal"
									type="url"
									fullWidth
									value={
										this.state.send.hasOwnProperty('website')
											?
										this.state.send.website
											:
										this.state.form.website 
											? 
										this.state.form.website 
											: 
										''
									}
									onChange={this.handleChange('website')}
								/>
									:
								null
							}
						</Grid>
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid container>
								<Grid 
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<SelectLocalities 
										id="modify/born"
										label="¿Donde naciste?"
										value={
											this.state.send.hasOwnProperty('bornLocalities')
												?
											this.state.send.bornLocalities
												:
											this.state.form.bornLocalities 
												? 
											this.state.form.bornLocalities 
												: 
											''
										}
										onChange={this.handleChange('bornLocalities')}
									/>
								</Grid>
								<Grid 
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<SelectLocalities 
										id="modify/live"
										label="¿Donde vives?"
										value={
											this.state.send.hasOwnProperty('liveLocalities')
											?
											this.state.send.liveLocalities
												:
											this.state.form.liveLocalities 
												? 
											this.state.form.liveLocalities 
												: 
											''
										}
										onChange={this.handleChange('liveLocalities')}
									/>
								</Grid>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Lenguajes Nativo</Typography>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
							>
								<SelectLanguageNative 
									id="modify/languageNative"
									limit={2}
									delete={this.state.form.languageNative ? false : true}
									value={
										this.state.send.hasOwnProperty('languageNative')
											?
										this.state.send.languageNative
											:
										this.state.form.languageNative 
											? 
										this.state.form.languageNative 
											: 
										''
									}
									onChange={this.handleChange('languageNative')}
									firebase={this.props.firebase}
								/>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Lenguajes</Typography>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<SelectLanguage 
									id="modify/language"
									delete={this.state.form.language ? false : true}
									value={
										this.state.send.hasOwnProperty('language')
											?
										this.state.send.language
											:
										this.state.form.language 
											? 
										this.state.form.language 
											: 
										''
									}
									onChange={this.handleChange('language')}
									firebase={this.props.firebase}
								/>
							</Grid>
								:
							null
						}
						<Grid
							container
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
							justify="center"
						>
							<Grid
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
							>
								<UploadButton 
									value={
										this.state.send.hasOwnProperty('file')
											?
										this.state.send.file
											:
										this.state.form.photo
											?
										this.state.form.photo
											:
										''
									}
									delete={this.state.form.file ? false : true} 
									onChange={event => 
										this.setState({
											send: {
												...this.state.send,
												file: event.target.value,
												modifyFile: true
											}	
										})
									}
									labelButton="Subir imagen" 
									labelButtonSuccess="Ver imagen" 
									labelDialog="Subir image de perfil"
									style={
										{
											textAlign: 'center',
											paddingTop: 50,
											paddingBottom: 20
										}
									}
								/>
							</Grid>
						</Grid>
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item 
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Herramientas de Comunicación</Typography>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item 
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="modify/tool-skype"
									label="Skype"
									margin="normal"
									fullWidth
									type="text"
									value={
										this.state.send.tools.hasOwnProperty('skype')
											? 
										this.state.send.tools.skype
											: 
										this.state.form.tools.hasOwnProperty('skype')
											? 
										this.state.form.tools.skype 
											: 
										''
									}
									onChange={this.handleChangeTools('skype')}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<FontAwesome.FaSkype/>
											</InputAdornment>
										)
									}}
								/>
								<TextField
									id="modify/tool-facetime"
									label="FaceTime"
									margin="normal"
									fullWidth
									type="text"
									value={
										this.state.send.tools.hasOwnProperty('facetime')
											? 
										this.state.send.tools.facetime
											: 
										this.state.form.tools.hasOwnProperty('facetime')
											? 
										this.state.form.tools.facetime 
											: 
										''
									}
									onChange={this.handleChangeTools('facetime')}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<Icon>videocam</Icon>
											</InputAdornment>
										)
									}}
								/>
								<TextField
									id="modify/tool-qqhangouts"
									label="Google Hangouts"
									margin="normal"
									fullWidth
									type="text"
									value={
										this.state.send.tools.hasOwnProperty('gHangouts')
											? 
										this.state.send.tools.gHangouts
											: 
										this.state.form.tools.hasOwnProperty('gHangouts')
											? 
										this.state.form.tools.gHangouts 
											: 
										''
									}
									onChange={this.handleChangeTools('gHangouts')}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<Icon>format_quote</Icon>
											</InputAdornment>
										)
									}}
								/>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid 
								item
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="modify/tool-wechat"
									label="WeChat"
									margin="normal"
									fullWidth
									type="text"
									value={
										this.state.send.tools.hasOwnProperty('wechat')
											? 
										this.state.send.tools.wechat
											: 
										this.state.form.tools.hasOwnProperty('wechat')
											? 
										this.state.form.tools.wechat 
											: 
										''
									}
									onChange={this.handleChangeTools('wechat')}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<FontAwesome.FaWechat/>
											</InputAdornment>
										)
									}}
								/>
								<TextField
									id="modify/tool-qq"
									label="Tencent QQ"
									margin="normal"
									fullWidth
									type="text"
									value={
										this.state.send.tools.hasOwnProperty('qq')
											? 
										this.state.send.tools.qq 
											: 
										this.state.form.tools.hasOwnProperty('qq')
											? 
										this.state.form.tools.qq 
											: 
										''
									}
									onChange={this.handleChangeTools('qq')}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<FontAwesome.FaQq/>
											</InputAdornment>
										)
									}}
								/>
							</Grid>
								:
							null
						}
						<button type="submit" id="modify/submit" style={{display: 'none'}}>Enviar</button>
					</Grid>
				</form>
			</Paper>
		)
	}
}

export default Modify;
