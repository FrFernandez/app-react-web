import React, { Component } from 'react';
import { Paper, Tooltip } from '@material-ui/core';

import { TableCustom } from '../../utils';

const headCollection = [
	{
		title: '#',
		tooltip: false
	},
	{
		title: 'Imagen',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
	},
	{
		title: 'Nombre de Usuario',
		tooltip: true,
		position: 'bottom-end',
		delay: 300
	},
	{
		title: 'Nombre Completo',
		tooltip: true,
		position: 'bottom-end',
		delay: 300
	},
	{
		title: 'Correo Electronico',
		tooltip: true,
		position: 'bottom-end',
		delay: 300
	},
	{
		title: 'Privilegios',
		tooltip: true,
		position: 'bottom-end',
		delay: 300
	},
]

class List extends Component {
	state = {
		users: []
	}
	constructor(props){
		super()

		this.firestore = props.firebase.firestore().collection('users');
	}
	componentWillMount(){
		this.firestore
			.onSnapshot(snapshot => {
				this.setState({ users: snapshot.docs })
			}, error => console.log(error))
	}
	render(){
		const { users } = this.state;
		const { 
			handleChange, 
			id, 
			value 
		} = this.props;
		return (
			<Paper style={{
				width: '100%',
				overflowX: 'auto',
			}}>
				<div style={{ display: value === id ? 'block' : 'none' }}>
					<TableCustom
						headCollection={headCollection}
						bodyCollection={users}
						handleEdit={handleChange}
						handleDelete={this.props.handleDelete}
						handleView={this.props.handleView}
						footer=""
						style={{
							minWidth: 700,
						}}
						template="users"
					/>
				</div>
			</Paper>
		)
	}
}

export default List;