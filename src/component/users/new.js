import React, { Component } from 'react';
import {
	MenuItem,
	TextField,
	Typography,
	Paper,
	Grid,
	Icon,
	IconButton, 
	FormControl,
	Input,
	InputLabel,
	InputAdornment
} from '@material-ui/core';
import * as FontAwesome from 'react-icons/lib/fa';

import {
  SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguage,
	SelectLanguageNative,
	UploadButton,
	DateNow
} from '../../utils';

class New extends Component {
	state = {
		open: false,
		form: {
			tools: {},
		},
		showpassword: false
	}
	constructor(props){
		super(props);

		this.firestore = props.firebase.firestore().collection('users');
		this.storage   = props.firebase.storage();
		this.onSubmit  = this.onSubmit.bind(this)
		this.handleChange  = this.handleChange.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
		this.props.handleOnClick(this.handleOnClick)
	}
	handleChange = name => event => {
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	UploadFile = (userId, mes) => {
		let type = this.state.form.file.type.split('/')[1];
		const storage  = this.storage.ref(),
					route    = `users/${userId}/thumbnails/${userId}.${type}`,
					photo    = storage.child(route),
					task     = photo.put(this.state.form.file); 
		task.then(snapshot => {
			this.firestore
					.doc(userId)
					.update({ photo : task.snapshot.downloadURL, photoRef: route})
					.then(response => {
						this.props.handleSnackbar({type: 'custom', message: mes})
						setTimeout(() => {
							this.setState({ 
								form: {
									tools: {}
								} 
							})
							this.props.handleOnLoading('hide')
						}, 2500)
						this.props.handleOnLoading('done')
					})
					.catch(error => {
						console.log(error)
						this.props.handleOnLoading('hide')
						return this.props.handleSnackbar({ type: 'custom', error:  error ? error : '', on: true})
					})			
		})
		.catch(error => {
			if(error.errorInfo)
				return this.props.handleSnackbar({ type: 'storage', error:  error.errorInfo.code, on: true})
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})	
	}
	onSubmit = event => {
		event.preventDefault();
		this.props.handleOnLoading('show')
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/users/',
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'POST',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token, 
					...this.state.form
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			this.props.handleOnLoading('hide')
			if(response.success === true){
				if(this.state.form.file)
					this.UploadFile(response.id, response.mes) 
				else
					this.props.handleSnackbar({type: 'custom', message: response.mes})
					setTimeout(() => {
						this.setState({ 
							form: {
								tools: {}
							} 
						})
						this.props.handleOnLoading('hide')
					}, 2500)
					this.props.handleOnLoading('done')
			}
			else {
				this.props.handleOnLoading('hide')
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleOnLoading('hide')
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleOnClick = () => {
		document.getElementById('new/submit').click()
	}
	render() {
		const { id, value } = this.props;
		const { showpassword } = this.state;
		return (
			<Paper style={{ padding: 15 }}>
				<form 
					id="new"
					onSubmit={this.onSubmit} 
					style={{ display: value === id ? 'block' : 'none' }}
				>
					<Typography variant="title" gutterBottom>Nuevo usuario</Typography>
					<Grid container>
					<Grid
							item 
							xs={12}
							sm={12}
							md={12}
							lg={12}
							xl={12}
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							<Typography variant="body2" style={{marginTop: 10}}>Datos del usuario</Typography>
						</Grid>
						<Grid 
							item 
							xs={12} 
							sm={12} 
							md={6} 
							lg={6} 
							xl={6} 
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							<TextField
								id='new/username'
								label="Nombre de usuario"
								margin="normal"
								fullWidth
								// required
								// error={this.state.error.username}
								type='text'
								value={this.state.form.username ? this.state.form.username : ''}
								onChange={this.handleChange('username')}
							/>
							<TextField
								id="new/mail"
								label="Correo electronico"
								margin="normal"
								fullWidth
								// required
								// error={this.state.error.email}
								value={this.state.form.email ? this.state.form.email : ''}
								type='email'
								onChange={this.handleChange('email')}
							/>
							<FormControl
								margin="normal"
								fullWidth
							>
								<InputLabel htmlFor="password">Contraseña</InputLabel> 	
								<Input
									id="new/password"
									label="Contraseña"
									// required
									type={ showpassword ? 'text' : 'password' }
									value={this.state.form.password ? this.state.form.password : ''}
									onChange={this.handleChange('password')}
									endAdornment={
										<InputAdornment position="end">
											<IconButton
												aria-label="Toggle password visibility"
												onClick={
													event => this.setState((prevState, props) => {
														return {showpassword: !prevState.showpassword}
													}
												)}
												onMouseDown={event => event.preventDefault()}
											>
												<Icon>{ showpassword ? 'visibility' : 'visibility_off' }</Icon>
											</IconButton>
										</InputAdornment>
									}
								/>
							</FormControl>
							<TextField
								id="new/priv"
								select
								label="Privilegio"
								// required
								value={this.state.form.priv ? this.state.form.priv : ''}
								onChange={this.handleChange('priv')}
								fullWidth
								margin="normal"
							>
								<MenuItem key={1} value={1}>
									Administrador
								</MenuItem>
								<MenuItem key={2} value={2}>
									Supervisor
								</MenuItem>
								<MenuItem key={3} value={3}>
									Usuario
								</MenuItem>
							</TextField>
							{
								this.state.form.priv !== 1 && this.state.form.priv !== 2
									?
								<SelectCurrentMoney 
									id="new/money" 
									label="Moneda"
									delete={this.state.form.money ? false : true}
									onChange={this.handleChange('money')}
									firebase={this.props.firebase}
								/>
									:
								null
							}
							<TextField
								id="new/watch"
								select
								label="Tipo hora"
								// required
								value={this.state.form.watch ? this.state.form.watch : ''}
								onChange={this.handleChange('watch')}
								fullWidth
								margin="normal"
							>
								<MenuItem key={1} value={1}>
									24Hrs
								</MenuItem>
								<MenuItem key={2} value={2}>
									12Hrs
								</MenuItem>
							</TextField>
							<Grid container>
								<Grid
									item
									xs={12}	
									ms={12}	
									md={8}	
									lg={8}	
									xl={8}	
								>
									<SelectTimeZone 
										id="new/timezone"
										label="Zona horaria"
										value={this.state.form.timezone ? this.state.form.timezone : ''}
										onChange={this.handleChange('timezone')}
									/>
								</Grid>
								<Grid
									item
									xs={12}	
									ms={12}	
									md={4}	
									lg={4}	
									xl={4}	
								>
									<DateNow
										timezone={this.state.form.timezone}
										watch={this.state.form.watch}
									/>
								</Grid>
							</Grid>
							
						</Grid>
						<Grid 
							item 
							xs={12} 
							sm={12} 
							md={6} 
							lg={6} 
							xl={6} 
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							<TextField
								id="new/fullname"
								label="Nombre completo"
								margin="normal"
								fullWidth
								// required
								// error={false}
								type='text'
								value={this.state.form.fullname ? this.state.form.fullname : ''}
								inputProps={{
									pattern: '[A-Za-z ]{1,}',
									title: 'Solo se aceptan letras'
								}}
								onChange={this.handleChange('fullname')}
							/>
							<TextField
								id="new/mobile"
								label="Telefono movil"
								placeholder="+581234567890"
								margin="normal"
								fullWidth
								// required
								// error={false}
								type='phone'
								inputProps={{
									pattern: '[+][0-9]{12}',
									title: 'Solo se aceptan letras'
								}}
								value={this.state.form.mobile ? this.state.form.mobile : ''}
								onChange={this.handleChange('mobile')}
							/>
							<TextField
								id="new/phone"
								label="Telefono casa"
								placeholder="+581234567890"
								margin="normal"
								fullWidth
								// required
								// error={false}
								type='phone'
								inputProps={{
									pattern: '[+][0-9]{12}',
									title: 'Solo se aceptan letras'
								}}
								value={this.state.form.phone ? this.state.form.phone : ''}
								onChange={this.handleChange('phone')}
							/>
							<TextField
								id="new/gender"
								select
								label="Genero"
								// required
								value={this.state.form.gender ? this.state.form.gender : ''}
								onChange={this.handleChange('gender')}
								fullWidth
								margin="normal"
							>
								<MenuItem key={'M'} value={'M'}>
									Masculino
								</MenuItem>
								<MenuItem key={'F'} value={'F'}>
									Femenino
								</MenuItem>
							</TextField>
							<TextField
								id="new/birthdate"
								label="Fecha de cumpleaños"
								margin="normal"
								fullWidth
								// required
								type='date'
								InputLabelProps={{
									shrink: true,
								}}
								value={this.state.form.birthdate ? this.state.form.birthdate : ''}
								onChange={this.handleChange('birthdate')}
							/>
							<TextField
								id="new/address"
								label="Direccion"
								multiline
								fullWidth
								margin="normal"
								value={this.state.form.address ? this.state.form.address : ''}
								onChange={this.handleChange('address')}
							/>
							{
								this.state.form.priv !== 1 && this.state.form.priv !== 2
									?
								<TextField
									id="new/company"
									label="Compañia"
									margin="normal"
									type="text"
									fullWidth
									value={this.state.form.company ? this.state.form.company : ''}
									onChange={this.handleChange('company')}
								/>
									:
								null
							}
						</Grid>
						<Grid
							item 
							xs={12}
							sm={12}
							md={12}
							lg={12}
							xl={12}
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
						>
							{
								this.state.form.priv !== 1 && this.state.form.priv !== 2
									?
								<TextField
									id="new/website"
									label="Sitio web"
									margin="normal"
									type="url"
									fullWidth
									value={this.state.form.website ? this.state.form.website : ''}
									onChange={this.handleChange('website')}
								/>
									:
								null
							}
						</Grid>
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid container>
								<Grid 
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<SelectLocalities 
										id="new/born"
										label="¿Donde naciste?"
										value={this.state.form.bornLocalities ? this.state.form.bornLocalities : ''}
										onChange={this.handleChange('bornLocalities')}
									/>
								</Grid>
								<Grid 
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<SelectLocalities 
										id="new/live"
										label="¿Donde vives?"
										value={this.state.form.liveLocalities ? this.state.form.liveLocalities : ''}
										onChange={this.handleChange('liveLocalities')}
									/>
								</Grid>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?	
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Lenguajes Nativo</Typography>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
							>
								<SelectLanguageNative 
									id="new/languageNative"
									limit={2}
									delete={this.state.form.languageNative ? false : true}
									onChange={this.handleChange('languageNative')}
									firebase={this.props.firebase}
								/>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Lenguajes</Typography>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<SelectLanguage 
									id="new/language"
									delete={this.state.form.language ? false : true}
									onChange={this.handleChange('language')}
									firebase={this.props.firebase}
								/>
							</Grid>
								:
							null
						}
						<Grid
							container
							style={{
								paddingLeft: 10,
								paddingRight: 10
							}}
							justify="center"
						>
							<Grid
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
							>
								<UploadButton 
									value={this.state.form.file ? this.state.form.file : ''} 
									onChange={this.handleChange('file')}
									labelButton="Subir imagen" 
									labelButtonSuccess="Ver imagen" 
									labelDialog="Subir image de perfil"
									style={
										{
											textAlign: 'center',
											paddingTop: 50,
											paddingBottom: 20
										}
									}
								/>
							</Grid>
						</Grid>
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item 
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Herramientas de Comunicación</Typography>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid
								item 
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="new/tool-skype"
									label="Skype"
									margin="normal"
									fullWidth
									type="email"
									value={this.state.form.tools.skype ? this.state.form.tools.skype : ''}
									onChange={event => {
										this.setState({
											form: {
												...this.state.form,
												tools: {
													...this.state.form.tools,
													skype: event.target.value
												}
											}
										})
									}}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<FontAwesome.FaSkype/>
											</InputAdornment>
										)
									}}
								/>
								<TextField
									id="new/tool-facetime"
									label="FaceTime"
									margin="normal"
									fullWidth
									type="email"
									value={this.state.form.tools.facetime ? this.state.form.tools.facetime : ''}
									onChange={event => {
										this.setState({
											form: {
												...this.state.form,
												tools: {
													...this.state.form.tools,
													facetime: event.target.value
												}
											}
										})
									}}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<Icon>videocam</Icon>
											</InputAdornment>
										)
									}}
								/>
								<TextField
									id="new/tool-qqhangouts"
									label="Google Hangouts"
									margin="normal"
									fullWidth
									type="email"
									value={this.state.form.tools.gHangouts ? this.state.form.tools.gHangouts : ''}
									onChange={event => {
										this.setState({
											form: {
												...this.state.form,
												tools: {
													...this.state.form.tools,
													gHangouts: event.target.value
												}
											}
										})
									}}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<Icon>format_quote</Icon>
											</InputAdornment>
										)
									}}
								/>
							</Grid>
								:
							null
						}
						{
							this.state.form.priv !== 1 && this.state.form.priv !== 2
								?
							<Grid 
								item
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="new/tool-wechat"
									label="WeChat"
									margin="normal"
									fullWidth
									type="email"
									value={this.state.form.tools.wechat ? this.state.form.tools.wechat : ''}
									onChange={event => {
										this.setState({
											form: {
												...this.state.form,
												tools: {
													...this.state.form.tools,
													wechat: event.target.value
												}
											}
										})
									}}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<FontAwesome.FaWechat/>
											</InputAdornment>
										)
									}}
								/>
								<TextField
									id="new/tool-qq"
									label="Tencent QQ"
									margin="normal"
									fullWidth
									type="email"
									value={this.state.form.tools.qq ? this.state.form.tools.qq : ''}
									onChange={event => {
										this.setState({
											form: {
												...this.state.form,
												tools: {
													...this.state.form.tools,
													qq: event.target.value
												}
											}	
										})
									}}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<FontAwesome.FaQq/>
											</InputAdornment>
										)
									}}
								/>
							</Grid>
								:
							null
						}
						<button type="submit" id="new/submit" style={{display: 'none'}}>Enviar</button>
					</Grid>
				</form>
			</Paper>
		)
	}
}

export default New;