import React, { Component } from 'react';
import {
  Icon,
  Button,
  ButtonBase
} from '@material-ui/core';
import { 
  FaFacebookSquare,
  FaTwitterSquare,
  FaYoutubeSquare  
} from 'react-icons/lib/fa';
import './index.css';
import moment from 'moment';

class Landing extends Component {
  state = {
    contentInfo: [true],
    lessionCard: [true],
    teacherCard: [true],
    active: '',
    collapse: false,
  }
  constructor(){
    super();

    this.handleScroll = this.handleScroll.bind(this);

    setTimeout(() => this.setState({active: 'active'}), 800);
  }
  componentDidMount() {
		document.title = 'SpeakEnglishSite'
		window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll = event => {
    let navbar    = document.getElementById('navbar'),
        container = navbar.getElementsByClassName('container-fluid')[0];
        
    let positionScroll = document.documentElement.scrollTop;
    if(positionScroll > 100)
      navbar.setAttribute("active", true);
    else 
      navbar.setAttribute("active", false);

    this.handleAnimation(positionScroll)
  }
  handleAnimation = positionScroll => {
    let windowHeight                  = window.outerHeight / 2,
        spLession                     = document.getElementById('sp-lession'), 
        spLeesionScrollPosition       = spLession.offsetTop,
        spOffer                       = document.getElementById('sp-offer'),
        spOfferScrollPosition         = spOffer.offsetTop,
        spNeed                        = document.getElementById('sp-need'),
        spNeedScrollPosition          = spNeed.offsetTop,
        spTeacherChoose               = document.getElementById('sp-teacher-choose'),
        spTeacherChooseScrollPosition = spTeacherChoose.offsetTop;


    if(positionScroll >= Math.abs(windowHeight - spLeesionScrollPosition)) {
      spLession.setAttribute('animation', true)
    } 
    if(positionScroll >= Math.abs(windowHeight - spOfferScrollPosition)) {
      spOffer.setAttribute('animation', true)
    }
    if(positionScroll >= Math.abs(windowHeight - spNeedScrollPosition)) {
      spNeed.setAttribute('animation', true)
    }
    if(positionScroll >= Math.abs(windowHeight - spTeacherChooseScrollPosition)){
      spTeacherChoose.setAttribute('animation', true)
    }
  }
  render(){
    const { 
      contentInfo, 
      lessionCard, 
      teacherCard, 
      active, 
      collapse 
    } = this.state; 
    return (
      <div>
        <header id="navbar" className={`${collapse ? 'open' : ''}`}>
          <div className="container-fluid">
            <ButtonBase className={`navbar-icon ${collapse ? 'open' : ''}`} onClick={() => this.setState({ collapse: !collapse })}>
              <span className="bar"></span>
              <span className="bar"></span>
              <span className="bar"></span>
            </ButtonBase>
            <div className={`navbar-collapse ${collapse ? 'open' : ''}`}>
              <div className="nav-item">
                <ButtonBase>
                  <a href="#sp-home">Inicio</a>
                </ButtonBase>
              </div>
              <div className="nav-item">
                <ButtonBase>
                  <a href="#sp-lession">¿Cómo funciona?</a>
                </ButtonBase>
              </div>
              <div className="nav-item">
                <ButtonBase>
                  <a href="#sp-teacher-choose">¡Postúlate como Profesor!</a>
                </ButtonBase>
              </div>
            </div>
            <div className="blankspace"></div>
            <div className="nav-item">
              <Button 
                variant="raised"
                className="sp-btn"
                onClick={() => window.location.href = '/auth'}
              >
                Iniciar Sesíon
              </Button>
            </div>
            <div className="nav-item">
              <Button 
                variant="raised"
                className="sp-btn"
                onClick={() => window.location.href = '/auth'}
              >
                Registrate
              </Button>
            </div>
          </div>
        </header>
        <section id="sp-home">
          <div className="container">
            <div className={`row animation ${active}`}>
              <div className={`content-info ${contentInfo[0] ? 'animate-show' : 'fadeLeft'}`}> 
                <h1 className="title-info">Clases en vivo con expertos.</h1>
                <p className="text-info">
                  ¡Es fácil!!. Simplemente abre tu computadora y reserva tu clase, aprende desde casa en cualquier momento. 
                </p>
                <Button className="btn-info">¡Empieza Ahora!</Button>
              </div>
              <div className={`content-info ${contentInfo[1] ? 'animate-show' : 'fadeLeft'}`}>
                <h1 className="title-info">Toma clases de ingles en linea 1-1 con nuestros profesores nativos.</h1>
                <Button className="btn-info">¡Empieza Ahora!</Button>
              </div>
              <div className={`content-info ${contentInfo[2] ? 'animate-show' : 'fadeLeft'}`}>
                <h1 className="title-info">Solicita enseñar.</h1>
                <p className="text-info">
                  Conviértase en un profesor y gane dinero trabajando desde cualquier lugar y momento. 
                </p>
                <Button className="btn-info">¡Comienza a enseñar ahora!</Button>
              </div>
              <div className="btn-control">
                <ul>
                  <li>
                    <a className={`pointer ${contentInfo[0] ? 'active' : ''}`} onClick={() => this.setState({contentInfo: [true, false, false]})}></a>
                  </li>
                  <li>
                    <a className={`pointer ${contentInfo[1] ? 'active' : ''}`} onClick={() => this.setState({contentInfo: [false, true, false]})}></a>
                  </li>
                  <li>
                    <a className={`pointer ${contentInfo[2] ? 'active' : ''}`} onClick={() => this.setState({contentInfo: [false, false, true]})}></a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="row logo">
              <div className="circle">
                <div className="buho">
                  <img src="./buho.svg" alt=""/>
                </div>
              </div>
              <div className="title">
                <h1>SpeakEnglishSite</h1>
              </div>
            </div>
            <div className="arrow-down">
              <a href="#sp-lession">
                <img src="./arrow-down.svg" alt=""/>
              </a>
            </div>
          </div>
        </section>
        <section id="card-info">
          <div className="container">
            <div className={`content-info ${contentInfo[0] ? 'animate-show' : 'fadeLeft'}`}> 
              <h1 className="title-info">Clases en vivo con expertos.</h1>
              <p className="text-info">
                ¡Es fácil!!. Simplemente abre tu computadora y reserva tu clase, aprende desde casa en cualquier momento. 
              </p>
              <Button className="btn-info">¡Empieza Ahora!</Button>
            </div>
            <div className={`content-info ${contentInfo[1] ? 'animate-show' : 'fadeLeft'}`}>
              <h1 className="title-info">Toma clases de ingles en linea 1-1 con nuestros profesores nativos.</h1>
              <Button className="btn-info">¡Empieza Ahora!</Button>
            </div>
            <div className={`content-info ${contentInfo[2] ? 'animate-show' : 'fadeLeft'}`}>
              <h1 className="title-info">Solicita enseñar.</h1>
              <p className="text-info">
                Conviértase en un profesor y gane dinero trabajando desde cualquier lugar y momento. 
              </p>
              <Button className="btn-info">¡Comienza a enseñar ahora!</Button>
            </div>
            <div className="btn-control">
              <ul>
                <li>
                  <a className={`pointer ${contentInfo[0] ? 'active' : ''}`} onClick={() => this.setState({contentInfo: [true, false, false]})}></a>
                </li>
                <li>
                  <a className={`pointer ${contentInfo[1] ? 'active' : ''}`} onClick={() => this.setState({contentInfo: [false, true, false]})}></a>
                </li>
                <li>
                  <a className={`pointer ${contentInfo[2] ? 'active' : ''}`} onClick={() => this.setState({contentInfo: [false, false, true]})}></a>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section id="sp-lession">
          <div className="container">
            <div className="title">
              <h1>
                ¿Como reservar una leccion?
                <hr/>
              </h1>
            </div>
            <div className="content carousel-cards">
              <div className="row text-content">
                <div className={`text-card ${lessionCard[0] ? 'show' : ''}`}>
                  <div className="title">
                    <span className="badge">1</span>
                    <h3>Elige tu profesor.</h3>
                  </div>
                  <div className="description">
                    <p>Lea su experiencia y vea su presentaciones de video para seleccionar lo mejor para sus necesidades.</p>
                  </div>
                </div>
                <div className={`text-card ${lessionCard[1] ? 'show' : ''}`}>
                  <div className="title">
                    <span className="badge">2</span>
                    <h3>Elige una fecha y hora.</h3>
                  </div>
                  <div className="description">
                    <p>Elige un día y hora más conveniente para usted.</p>
                  </div>
                </div>
                <div className={`text-card ${lessionCard[2] ? 'show' : ''}`}>
                  <div className="title">
                    <span className="badge">3</span>
                    <h3>Conectate.</h3>
                  </div>
                  <div className="description">
                    <p>Toma tu lección usando Skype, Hangouts o cualquier otra plataforma de VOIP</p>
                  </div>
                </div>
              </div>
              <div className="row cards-content">
                <div className={`card card_1 ${lessionCard[0] ? 'active' : ''}`}></div>
                <div className={`card card_2 ${lessionCard[1] ? 'active' : ''}`}></div>
                <div className={`card card_3 ${lessionCard[2] ? 'active' : ''}`}></div>
                <div className="btn-control">
                  <ul>
                    <li>
                      <a className={`pointer ${lessionCard[0] ? 'active' : ''}`} onClick={() => this.setState({lessionCard: [true, false, false]})}></a>
                    </li>
                    <li>
                      <a className={`pointer ${lessionCard[1] ? 'active' : ''}`} onClick={() => this.setState({lessionCard: [false, true, false]})}></a>
                    </li>
                    <li>
                      <a className={`pointer ${lessionCard[2] ? 'active' : ''}`} onClick={() => this.setState({lessionCard: [false, false, true]})}></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="sp-offer">
          <div className="container">
            <div className="title">
              <h1>
                ¿Que ofrecemos?
                <hr/>
              </h1>
            </div>
            <div className="content">
              <div className="row">
                <div className="card-blank">
                  <div className="content-circle">
                    <div className="circle"></div>
                    <div className="text-content">
                      <h2>Clases personales</h2>
                      <p>Es simple, tomar clases personales con el método de uno a uno. Domina tus habilidades linguísticas que necesitas para el éxito. </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="card-blank">
                  <div className="content-circle">
                    <div className="circle"></div>
                    <div className="text-content">
                      <h2>Su horario</h2>
                      <p>
                        Nuestro maestros están disponibles en cualquier momento. 
                        Puedes elegir cualquier maestro y reservar tu clase. Le darán retroalimentación para mejorar sus habilidades sobre la pronunciación, la fluidez, el vocabulario o cualquier tema que necesites. Te están esperando.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="card-blank">
                  <div className="content-circle">
                    <div className="circle"></div>
                    <div className="text-content">
                      <h2>Tus objectivos</h2>
                      <p>
                        Después de la clases, hablarás inglés perfecto. Podrá hablar inglés fluido en cualquier situación. Además, pude solicitar a nuestros profesores que dirijan sus estudios a cursos especializados para negocios, viajes y presentaciones.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="sp-need">
          <div className="container">
            <div className="title">
              <h1>
                ¿Cuáles son tus necesidades?
                <hr/>
              </h1>
            </div>
            <div className="content">
              <div className="row">
                <div className="card-blank">
                  <div className="content-circle">
                    <div className="circle"></div>
                    <div className="text-content">
                      <h2>Negocio</h2>
                      </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="card-blank">
                  <div className="content-circle">
                    <div className="circle"></div>
                    <div className="text-content">
                      <h2>Examenes de preparación</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="card-blank">
                  <div className="content-circle">
                    <div className="circle"></div>
                    <div className="text-content">
                      <h2>Viajes</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row line-p">
                <p>Cualquiera sea tu necesidad, estas en el lugar correcto.</p>
              </div>
            </div>
          </div>
        </section>
        <section id="sp-teacher-choose">
          <div className="container">
            <div className="title">
              <h1>
                Ventajas de ser profesor
                <hr/>
              </h1>
            </div>
            <div className="content carousel-cards">
              <div className="row cards-content">
                <div className={`card card_1 ${teacherCard[0] ? 'active' : ''}`}></div>
                <div className={`card card_2 ${teacherCard[1] ? 'active' : ''}`}></div>
                <div className={`card card_3 ${teacherCard[2] ? 'active' : ''}`}></div>
                <div className="btn-control">
                  <ul>
                    <li>
                      <a className={`pointer ${teacherCard[0] ? 'active' : ''}`} onClick={() => this.setState({teacherCard: [true, false, false]})}></a>
                    </li>
                    <li>
                      <a className={`pointer ${teacherCard[1] ? 'active' : ''}`} onClick={() => this.setState({teacherCard: [false, true, false]})}></a>
                    </li>
                    <li>
                      <a className={`pointer ${teacherCard[2] ? 'active' : ''}`} onClick={() => this.setState({teacherCard: [false, false, true]})}></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="row text-content">
                <div className={`text-card ${teacherCard[0] ? 'show' : ''}`}>
                  <div className="title">
                    <span className="badge">1</span>
                    <h3>Elige tu profesor.</h3>
                  </div>
                  <div className="description">
                    <p>Lea su experiencia y vea su presentaciones de video para seleccionar lo mejor para sus necesidades.</p>
                  </div>
                </div>
                <div className={`text-card ${teacherCard[1] ? 'show' : ''}`}>
                  <div className="title">
                    <span className="badge">2</span>
                    <h3>Elige una fecha y hora.</h3>
                  </div>
                  <div className="description">
                    <p>Elige un día y hora más conveniente para usted.</p>
                  </div>
                </div>
                <div className={`text-card ${teacherCard[2] ? 'show' : ''}`}>
                  <div className="title">
                    <span className="badge">3</span>
                    <h3>Conectate.</h3>
                  </div>
                  <div className="description">
                    <p>Toma tu lección usando Skype, Hangouts o cualquier otra plataforma de VOIP</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <footer>
          <div className="container">
            <div className="content">
              <div className="row">
                <h2>Estudiante</h2>
                <ul>
                  <li><a href="#">¿Cómo funciona?</a></li>
                  <li><a href="#">Encuentra un maestro.</a></li>
                </ul>
              </div>
              <div className="row">
                <h2>Maestro</h2>
                <ul>
                  <li><a href="#">Inicia sésion.</a></li>
                  <li><a href="#">Ser docente.</a></li>
                  <li><a href="#">Aplicar para enseñar.</a></li>
                  <li><a href="#">Administrar calendario.</a></li>
                  <li><a href="#">Pagos.</a></li>
                </ul>
              </div>
              <div className="row">
                <h2>Apoyo</h2>
                <ul>
                  <li><a href="#">Contacto.</a></li>
                  <li><a href="#">Preguntas frecuentes.</a></li>
                  <li><a href="#">Términos de uso</a></li>
                </ul>
              </div>
              <div className="row share">
                <h2>Síguenos</h2>
                <ul>
                  <li>
                    <a href="">
                      <FaFacebookSquare />
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <FaTwitterSquare />
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <FaYoutubeSquare />
                    </a>
                  </li>
                </ul>
              </div>
              <div className="row footer-small-button">
                <span>&copy; Copyright {moment().format('YYYY')}. Sitio de SpeakEnglishSite - Todos los derechos reservados. </span>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  } 
}

export default Landing;