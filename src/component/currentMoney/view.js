import React, { Component } from 'react';
import {
	Button,
	Dialog,
	AppBar,
	Toolbar,
	IconButton,
	Typography,
	Grid,
	Paper,
	TextField,
	FormControl,
	InputLabel,
	Input,
	InputAdornment,
	Icon,
	MenuItem,
	Slide,
	Avatar,
	List,
	ListSubheader,
	ListItem,
	ListItemIcon,
	ListItemText,
	Collapse,
	Divider,
	Card,
	CardMedia,
	CardContent,
	CardActions,
	CardHeader
} from '@material-ui/core';
import Google from 'react-icons/lib/fa/google';
import Facebook from 'react-icons/lib/fa/facebook-official';
import SwipeableViews from 'react-swipeable-views';
import * as FontAwesome from 'react-icons/lib/fa';
import {
	SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguage,
	SelectLanguageNative,
	UploadButton,
	DateNow,
	SnackbarLoading
} from '../../utils';

function Transition(props) {
	return <Slide direction="up" {...props} />;
}
class View extends Component {
	state = { 
		open: false, 
		uid: null, 
		user: null, 
		page: 0 
	}
	constructor(props){
		super(props)

		this.auth = this.props.firebase.auth;
		this.handleOpen  = this.handleOpen.bind(this)
		this.handleClose = this.handleClose.bind(this)
		this.props.handleOpen(this.handleOpen)
		this.props.handleClose(this.handleClose)
	}
	handleDataInfo = () => {
		this.state.onClickSnackLoadingOpen()
		fetch(
			`http://localhost:5000/admin-speakenglishsite/us-central1/api/users/${this.state.uid}
			?token=${this.props.user.auth.token}`
		)
		.then(response => response.json())
		.then(response => {
			if(response.success === true){
				this.setState({ 
					open: true, 
					user: response.item, 
					google: response.item.providerData.find(docs => docs.providerId === 'google.com' ? docs : null),
					facebook: response.item.providerData.find(docs => docs.providerId === 'facebook.com' ? docs : null)
				})
			}else {
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
			this.setState({ open: true })
		})
		.catch(error => {
			console.log(error)
			this.setState({ uid: null })
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
		.finally(() => this.state.onClickSnackLoadingClose())
	}
	handleOpen = uid => {
		if(uid !== this.state.uid)
			this.setState({ uid }, () => this.handleDataInfo())
		else if(uid === this.state.uid && this.state.user !== null)
			this.setState({ open: true })
		else if(uid === this.state.uid && this.state.user === null)
			this.handleDataInfo()
	}
	handlePage = page => event => {
		this.setState({ page })
	}
handleClose = () => this.setState({ open: false })
render(){
	const { page, showpassword } = this.state;	
	return(
		<div>
				<Dialog
					fullScreen
					open={this.state.open}
					onClose={this.handleClose}
					TransitionComponent={Transition}
				>
					<AppBar style={{position: 'relative'}}>
						<Toolbar>
							<IconButton
								color="inherit"
								onClick={this.handleClose}
							>
								<Icon>close</Icon>
							</IconButton>
							<Typography variant="title" color="inherit">
								Detalles del Usuario
							</Typography>
						</Toolbar>
					</AppBar>
					<div style={{ flexGrow: 1, padding: 16 }}>
						<Grid container>
							<Grid 
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
							>
								<Paper style={{ display: 'flex', position: 'relative', flexDirection: 'column' }}>
									<div style={{ display: 'flex', justifyContent: 'center',paddingTop: 16 }}>
										<Avatar
											style={{ width: 160, height: 160, boxShadow: '0 0 1px black' }} 
											alt={this.state.user ? this.state.user.username : ''}
											src={this.state.user ? this.state.user.photo : ''}
										/>
									</div>
									<List
										component="nav"
										style={{ paddingBottom: 0 }}
										subheader={
											<div>
												<ListSubheader component="div" style={{ textAlign: 'center', lineHeight: '36px', color: 'black', paddingBottom: 12 }}>
													{/* { this.state.user ? this.state.user.username : '' }
													<br/> */}
													{ this.state.user ? this.state.user.fullname : '' }
													<br/>
													{
														this.state.user ?
															this.state.user.priv === 1 ? 'Administrador'
																:
															this.state.user.priv === 2 ? 'Supervisor'
																:
															this.state.user.priv === 3 ? 'Usuario'
																:
															null
															:
														''
													}
												</ListSubheader>
											</div>
										}
									>
										<Divider/>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(0)} divider>
											<ListItemText primary="Información del Usuario"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(1)} divider>
											<ListItemText primary="Credenciales"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(2)} divider>
											<ListItemText primary="Idiomas"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(3)} divider>
											<ListItemText primary="Herramientas de Comunicación"></ListItemText>
										</ListItem>
									</List>
								</Paper>
							</Grid>
							<Grid 
								item
								xs={12}
								sm={12}
								md={9}
								lg={9}
								xl={9}
							>
								<div style={{padding: 15, paddingTop: 0}}>
									<SwipeableViews
										index={page}
										slideStyle={{
											alignItems: 'inherit',
											display: 'initial',
											padding: 5
										}}
									>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" gutterBottom>Información del Usuario</Typography>
											<Divider/>
											<Grid container spacing={8}>
												<Grid 
													item
													xl={6}
													lg={6}
													md={6}
													sm={12}
													xs={12}
												>
												</Grid>
											</Grid>
										</Paper>
										
										<div style={{ 
											padding: 15,
    									display: 'flex',
											position: 'relative',
											flexDirection: 'column' 
										}}>
										</div>
									</SwipeableViews>
								</div>
							</Grid>
						</Grid>
					</div>
				</Dialog>
				<SnackbarLoading 
					handleClick={event => this.setState({ onClickSnackLoadingOpen: event })} 
					handleClose={event => this.setState({ onClickSnackLoadingClose: event })} 
				/>
			</div>
		)
	}
}
	export default View;