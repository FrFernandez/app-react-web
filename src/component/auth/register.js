import React, { Component } from 'react';
import axios from 'axios';
import {
	TextField,
	Typography,
	Button,
	CircularProgress,
	MenuItem,
	Icon
} from '@material-ui/core';

import moment from 'moment';

class Register extends Component {
	state = {
		form: {},
		loading: 'Registrate',
	}
	constructor(props){
		super();

		this.auth 			   = props.firebase.auth();
		this.handleChange  = this.handleChange.bind(this);
		this.onSubmit      = this.onSubmit.bind(this);
		this.handleOnClick = this.handleOnClick.bind(this); 
	}
	handleChange = name => event => {
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	onSubmit = event => {
		event.preventDefault();
		this.props.handleOnLoading('show')

		let form = this.state.form;

		axios.post(`https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/auth/register`, form)
			.then(response => {
				console.log(response)
				if(response.data.success === true){
					return this.auth
						.signInWithEmailAndPassword(form.email, form.password)
						.then(user => {
							if(user) {
								this.setState({ form: {} })
								this.props.snackbarLoading();
							}
						})
						.catch(error => {
							console.log(error)
							if(!error)
								return this.props.snackbar({type: 'auth',message: error.code , on: true})
							return this.props.snackbar({type: 'custom', message: '404', on: true})
						})
				}else {
					if(response.data.error)
						return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
					return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
				}
			})
			.catch(error => {
				console.log(error.response)
				if(error.response.data && error.response.data.error)
					return this.props.handleSnackbar({ type: 'auth', message: error.response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
			})
	}
	handleOnClick = () => {
		document.getElementById('register').click()
	}
	render(){
		const { form } = this.state;
		return (
			<div className='form'>
				<form 
					id="register"
					onSubmit={this.onSubmit}
				>
					<Typography variant="title" gutterBottom>Registrate</Typography>
					<TextField
						id='username'
						label="Nombre de Usuario"
						margin="normal"
						fullWidth={true}
						required={true}
						type='text'
						value={form.username ? form.username : ''}
						onChange={this.handleChange('username')}
					/>
					<TextField
						id="register/mail"
						label="Correo Electronico"
						margin="normal"
						fullWidth={true}
						required={true}
						type='email'
						value={form.email ? form.email : ''}
						onChange={this.handleChange('email')}
					/>
					<TextField
						id="register/password"
						label="Contraseña"
						margin="normal"
						fullWidth={true}
						required={true}
						type='password'
						value={form.password ? form.password : ''}
						onChange={this.handleChange('password')}
					/>
					<TextField
						id="fullname"
						label="Nombre Completo"
						margin="normal"
						fullWidth={true}
						required={true}
						type='text'
						inputProps={{
							pattern: '[A-Za-z ]{1,}',
							title: 'Solo se aceptan letras'
						}}
						value={form.fullname ? form.fullname : ''}
						onChange={this.handleChange('fullname')}
					/>
					<TextField
						id="mobile"
						label="Telefono"
						placeholder="+581234567890"
						margin="normal"
						fullWidth={true}
						required={true}
						type='phone'
						inputProps={{
							pattern: '[+][0-9]{12}',
							title: 'Solo se aceptan letras'
						}}
						onChange={this.handleChange('mobile')}
					/>
					<TextField
						id="gender"
						select
						label="Select"
						required={true}
						fullWidth={true}
						margin="normal"
						value={form.gender ? form.gender : ''}
						onChange={this.handleChange('gender')}
					>
						<MenuItem key={'false'} value={'false'}>
							Selecciona un Genero
						</MenuItem>
						<MenuItem key={'M'} value={'M'}>
							Masculino
						</MenuItem>
						<MenuItem key={'F'} value={'F'}>
							Femenino
						</MenuItem>
						<MenuItem key={'O'} value={'O'}>
							Otros
						</MenuItem>
					</TextField>
					<TextField
						id="birthdate"
						label="Fecha de Cumpleaños"
						margin="normal"
						defaultValue={moment().format('YYYY-MM-DD')}
						fullWidth={true}
						required={true}
						error={false}
						type='date'
						onChange={event => this.setState({
							form: {
								...this.state.form,
								birthdate: event.target.value
							}
						})}
					/>
					<Button 
						type="submit"
						variant="raised" 
						fullWidth={true} 
						className="btn btn-signin"
						color='primary'
						onClick={this.handleOnClick}
					>
						{this.state.loading}
					</Button>
					<button type="submit" id="new/submit" style={{display: 'none'}}>Enviar</button>
				</form>
			</div>
		)
	}
}

export default Register;