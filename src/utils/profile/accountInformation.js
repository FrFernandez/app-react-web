import React,{ Component } from 'react';
import {
	Paper,
	Typography,
	Divider,
	Grid,
	TextField,
	MenuItem,
} from '@material-ui/core';
import {
	SelectLocalities,
	SelectCurrentMoney,
	SelectTimeZone,
	UploadButton
} from '../../utils';

class AccountInformation extends Component {
	state = { disabled: true, form: {} }
	constructor(props){
		super(props)
		
		this.firestore = props.firebase.firestore().collection('users');
		this.storage   = props.firebase.storage()
		this.handleOnClick = this.handleOnClick.bind(this)
		this.handleOnSubmit = this.handleOnSubmit.bind(this)
		this.props.handleOnEnabled(() => this.setState({ disabled: false }))
		this.props.handleOnSubmit(this.handleOnClick)
		this.props.handleCancel(() => this.setState({ disabled: true, form: {} }))
	}
	UploadFile = (userId, mes) => {
		let type = this.state.form.file.type.split('/')[1];
		const storage  = this.storage.ref(),
					route    = `users/${userId}/thumbnails/${userId}.${type}`,
					photo 	 = storage.child(route),
					task     = photo.put(this.state.form.file); 

		task.on('state_changed', 
			snapshot => {},
			err => console.log(`Error: ${err.code} ${err.message}`),
			() => {
				console.log(task.snapshot.downloadURL, route)
				this.firestore
						.doc(userId)
						.update({ photo : task.snapshot.downloadURL, photoRef: route })
						.then(response => {
							this.props.handleDone()
							setTimeout(() => this.props.handleReset(), 600)
							this.props.handleSnackbar({type: 'custom', message: mes})
						})
						.catch(error => {
							console.log(error)
							if(error.errorInfo)
								return this.props.handleSnackbar({ type: 'storage', error:  error.errorInfo, on: true})
							return this.props.handleSnackbar({ type: 'custom', message: error, on: true })
						}) 
			}
		)
	}
	handleOnSubmit = event => {
		event.preventDefault();
		this.props.handleLoading()
		console.log(this.state.form)
		fetch(
			'https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/auth',
			{
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'PUT',
				mode: 'cors',
				body: JSON.stringify({
					token: this.props.user.auth.token,
					...this.state.form
				})
			}
		)
		.then(response => response.json())
		.then(response => {
			if(response.success === true){
				if(this.state.form.file)
					this.UploadFile(response.id, response.mes)
				else
					this.setState({ disabled: true, form: {} })
					this.props.handleReset()
					this.props.handleSnackbar({type: 'custom', message: response.mes}) 
			}else {
				this.setState({ disabled: true, form: {} })
				this.props.handleReset()
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleReset()
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleChange = name => event => {
		if(this.state.form[name] === this.props.user.info[name])
			return;	
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	handleOnClick = () => {
		document.getElementById('profile/accountInformation').click()
	}
	render() {
		const { page, user } = this.props;
		const { disabled, form } = this.state;
		return (
			<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
				<div style={{ display: page === 0 ? 'block' : 'none' }}>
					<Typography variant="title" gutterBottom>Información del Usuario</Typography>
					<Divider/>
					<form
						onSubmit={this.handleOnSubmit}
					>
						<Grid container spacing={8}>
							<Grid container>
								<Grid 
									item
									xl={6}
									lg={6}
									md={6}
									sm={12}
									xs={12}
								>
									<TextField
										id="view/fullname"
										label="Nombre completo"
										className={disabled ? 'input-view' : ''}
										margin="normal"
										fullWidth
										disabled={disabled}
										type='text'
										value={
											form.hasOwnProperty('fullname')
												?
											form.fullname
												:
											user 
												? 
											user.info.fullname 
												: 
											''
										}
										onChange={this.handleChange('fullname')}
										inputProps={{
											pattern: '[A-Za-z ]{1,}',
											title: 'Solo se aceptan letras'
										}}
									/>
									<TextField
										id="new/gender"
										className={disabled ? 'input-view' : ''}
										select
										label="Genero"
										disabled={disabled}
										value={
											form.hasOwnProperty('gender')
												?
											form.gender 
												:
											user 
												? 
											user.info.gender 
												: 
											''
										}
										onChange={this.handleChange('gender')}
										fullWidth
										margin="normal"
									>
										<MenuItem key={'M'} value={'M'}>
											Masculino
										</MenuItem>
										<MenuItem key={'F'} value={'F'}>
											Femenino
										</MenuItem>
									</TextField>
									<TextField
										id="new/watch"
										className={disabled ? 'input-view' : ''}
										select
										label="Tipo hora"
										disabled={disabled}
										value={
											form.hasOwnProperty('watch')
												?
											form.watch
												:
											user 
												? 
											user.info.watch 
												: 
											''
										}
										onChange={this.handleChange('watch')}
										fullWidth
										margin="normal"
									>
										<MenuItem key={1} value={1}>
											24Hrs
										</MenuItem>
										<MenuItem key={2} value={2}>
											12Hrs
										</MenuItem>
									</TextField>
									<TextField
										id="new/address"
										label="Direccion"
										disabled={disabled}
										className={disabled ? 'input-view' : ''}
										multiline
										fullWidth
										margin="normal"
										value={
											form.hasOwnProperty('address')
												?
											form.address
												:
											user 
												? 
											user.info.address 
												: 
											''
										}
										onChange={this.handleChange('address')}
									/>
								</Grid>
								<Grid 
									item
									xl={6}
									lg={6}
									md={6}
									sm={12}
									xs={12}
								>
									<TextField
										id="new/birthdate"
										label="Fecha de cumpleaños"
										margin="normal"
										fullWidth
										disabled={disabled}
										className={disabled ? 'input-view' : ''}
										type='date'
										InputLabelProps={{
											shrink: true,
										}}
										value={
											form.hasOwnProperty('birthdate')
												?
											form.birthdate
												:
											user 
												? 
											user.info.birthdate 
												: 
											''
										}
										onChange={this.handleChange('birthdate')}
									/>
									<TextField
										id="new/phone"
										label="Telefono casa"
										className={disabled ? 'input-view' : ''}
										placeholder="+581234567890"
										margin="normal"
										fullWidth
										disabled={disabled}
										type='tel'
										inputProps={{
											pattern: '[+][0-9]{12}',
											title: 'Solo se aceptan letras'
										}}
										value={
											form.hasOwnProperty('phone')
												?
											form.phone
												:
											user 
												? 
											user.info.phone 
												: 
											''
										}
										onChange={this.handleChange('phone')}
									/>
									<SelectTimeZone 
										id="new/timezone"
										label="Zona horaria"
										disabled={disabled}
										className={disabled ? 'input-view' : ''}
										value={
											form.hasOwnProperty('timezone')
												?
											form.timezone
												:
											user 
												? 
											user.info.timezone 
												: 
											''
										}
										onChange={this.handleChange('timezone')}
									/>
									{
										user.info.priv !== 1 && user.info.priv !== 2
											?
										<SelectCurrentMoney 
											id="new/money"
											className={disabled ? 'input-view' : ''} 
											disabled={disabled}
											label="Moneda"
											value={
												form.hasOwnProperty('money')	
													?
												form.money
													:
												user 
													? 
												user.info.money 
													: 
												'' 
											}
											onChange={this.handleChange('money')}
											firebase={this.props.firebase}
										/>
											:
										null
									}
								</Grid>
							</Grid>
							{
								user.info.priv !== 1 && user.info.priv !== 2
									?
								<Grid container>
									<Grid 
										item
										xl={6}
										lg={6}
										md={6}
										sm={12}
										xs={12}
									>
										<TextField
											id="new/company"
											label="Compañia"
											margin="normal"
											disabled={disabled}
											className={disabled ? 'input-view' : ''}
											type="text"
											fullWidth
											value={
												form.hasOwnProperty('company')
													?
												form.company	
													:
												user 
													? 
												user.info.company 
													: 
												''
											}
											onChange={this.handleChange('company')}
										/>
									</Grid>
									<Grid 
										item
										xl={6}
										lg={6}
										md={6}
										sm={12}
										xs={12}
									>
										<TextField
											id="new/website"
											label="Sitio web"
											margin="normal"
											disabled={disabled}
											className={disabled ? 'input-view' : ''}
											type="url"
											fullWidth
											value={
												form.hasOwnProperty('website')
													?
												form.website
													:
												user 
													? 
												user.info.website 
													: 
												''
											}
											onChange={this.handleChange('website')}
										/>
									</Grid>
								</Grid>
									:
								null
							}
							{
								user.info.priv !== 1 && user.info.priv !== 2
									?
								<Grid container>
									<Grid 
										item
										xl={6}
										lg={6}
										md={6}
										sm={12}
										xs={12}
									>
										<SelectLocalities 
											id="new/born"
											label="¿Donde naciste?"
											disabled={disabled}
											className={disabled ? 'input-view' : ''}
											value={
												form.hasOwnProperty('bornLocalities')
													?
												form.bornLocalities
													:
												user 
													? 
												user.info.bornLocalities 
													: 
												''
											}
											onChange={this.handleChange('born')}
										/>
									</Grid>
									<Grid 
										item
										xl={6}
										lg={6}
										md={6}
										sm={12}
										xs={12}
									>
										<SelectLocalities 
											id="new/live"
											label="¿Donde vives?"
											disabled={disabled}
											className={disabled ? 'input-view' : ''}
											value={
												form.hasOwnProperty('liveLocalities')
													?
												form.liveLocalities
													:
												user 
													? 
												user.info.liveLocalities 
													: 
												''
											}
											onChange={this.handleChange('live')}
										/>
									</Grid>
								</Grid>
									:
								null
							}
							<Grid 
								item
								xs={12}
							>
								<UploadButton 
									value={
										form.hasOwnProperty('file')
											?
										form.file
											:
										user
											?
										user.info.photo
											:
										''
									} 
									onChange={event => 
										this.setState({
											form: {
												...form,
												file: event.target.value,
												modifyFile: true
											}	
										})
									}
									disabled={disabled}
									labelButton="Subir imagen" 
									labelButtonSuccess="Ver imagen" 
									labelDialog="Subir image de perfil"
								/>
							</Grid>
						</Grid>
						<button type="submit" id="profile/accountInformation" style={{display: 'none'}}>Enviar</button>
					</form>
				</div>
			</Paper>
		)
	}
}

export default AccountInformation;