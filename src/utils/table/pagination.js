import React, { Component } from 'react';
import { TablePagination } from '@material-ui/core';

class Pagination extends Component {
	state = {}
	render(){
		const { count, rowsPerPage, page, onChangePage, onChangeRowsPerPage } = this.props;
		return (
			<TablePagination
				component="div"
				count={count}
				rowsPerPage={rowsPerPage}
				page={page}
				onChangePage={onChangePage}
				onChangeRowsPerPage={onChangeRowsPerPage}
			/>
		)
	}
}

export default Pagination;