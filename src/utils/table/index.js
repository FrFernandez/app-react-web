import React, { Component } from 'react';
import { 
	Table,
	TableFooter 
} from '@material-ui/core';
import Head from './head';
import Body from './body';
import Pagination from './pagination';

class TableCustom extends Component {
	state = {
		page: 0,
		rowsPerPage: 5,
	}
	constructor(props) {
		super()

		this.onChangePage = this.onChangePage.bind(this)
		this.onChangeRowsPerPage = this.onChangeRowsPerPage.bind(this)
	}
	onChangePage = () => {

	}
	onChangeRowsPerPage = () => {

	}
	render(){
		const { 
			headCollection, 
			bodyCollection, 
			footer, 
			template, 
			style, 
			handleEdit, 
			handleDelete, 
			handleView
		} = this.props;
		return (
			<Table style={style}>
				<Head 
					collection={headCollection} 
					template={template}
				/>
				<Body 
					collection={bodyCollection} 
					template={template} 
					handleEdit={handleEdit} 
					handleDelete={handleDelete} 
					handleView={handleView}
					/>
				{
					footer
						? 
					<TableFooter>{footer}</TableFooter>
						:
					null
				}					
				{/* <Pagination
					count={bodyCollection.length}
					page={this.state.page}
					rowsPerPage={this.state.rowsPerPage}
					onChangePage={this.onChangePage}
					onChangeRowsPerPage={this.onChangeRowsPerPage}
				/> */}
			</Table>
		)
	}
}

export default TableCustom;