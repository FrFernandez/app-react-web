import React, { Component } from 'react';
import {
	TableHead,
	TableRow,
	TableCell,
	TableSortLabel, 
	Tooltip 
} from '@material-ui/core'; 

class Head extends Component {
	state = {}
	render(){
		const { collection } = this.props;
		return (
			<TableHead>
				<TableRow>
					{
						collection.map((docs, i) => (
							<TableCell 
								key={i}
								style={{
									paddingLeft: '20px',
									paddingRight: '20px'
								}}
							>
								{
									docs.tooltip 
										?
									<Tooltip
										title={docs.title}
										placement={docs.position}
										enterDelay={docs.delay}
									>
										<TableSortLabel>
											{docs.title}
										</TableSortLabel>
									</Tooltip>
										:
									docs.title	 
								}
							</TableCell>
						))
					}
					<TableCell>Acciones</TableCell>
				</TableRow>
			</TableHead>
		)
	}
}

export default Head;