import React,{ Component } from 'react';
import {
  Dialog,
  DialogTitle,
  Icon,
  Button,
  DialogActions,
  Slide,
  CircularProgress
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class DialogDelete extends Component {
  state = { open: false, disabled: false }
  constructor(props){
    super(props);

    this.handleDelete = this.handleDelete.bind(this)
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.props.handleOpen(this.handleOpen)
  }
  handleOpen = (event,uid) => {
    this.setState({ open: true, uid, event })
  }
  handleClose = () => {
    if(!this.state.disabled)
      this.setState({ open: false, uid: null })
  }
  handleDelete(){
    this.setState({ disabled: true })
    fetch(
      `https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/${this.props.url}/${this.state.uid}`,
      {
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: 'DELETE',
				mode: 'cors', 
				body: JSON.stringify({
					token: this.props.user.auth.token,
				})
			}
    )
    .then(response => response.json())
    .then(response => {
			if(response.success === true){
        this.setState({ disabled: false })
        this.props.handleSnackbar({type: 'custom', message: response.mes})
        this.handleClose()
			}else {
        this.setState({ disabled: false })
				if(response.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
  }
  render(){
    const { label } = this.props;
    const { disabled, event } = this.state;
    return (
      <div>
        <Dialog 
          open={this.state.open}
          TransitionComponent={Transition}
          onClose={this.handleClose}
        >
          <DialogTitle>Desea Eliminar {label}: {event}</DialogTitle>
          <DialogActions style={{ justifyContent: 'center', borderBottom: '1px solid #e1e1e1' }}>
            <div style={{ position: 'relative', margin: 8 }}>
              <Button
                disabled={disabled}
                onClick={this.handleDelete}
              >
                Eliminar
              </Button>
              {
                disabled 
                  && 
                <CircularProgress 
                  size={24} 
                  style={{
                    color: green[500],
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    marginTop: -12,
                    marginLeft: -12,
                  }} 
                />
              }
            </div>
            <Button 
              onClick={this.handleClose}
              disabled={disabled}
            >
              Cancelar
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default DialogDelete;