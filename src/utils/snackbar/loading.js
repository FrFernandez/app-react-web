import React,{ Component } from 'react';
import {
	Snackbar as SnackbarMaterial,
	IconButton,
	Icon,
	Slide,
	CircularProgress
} from '@material-ui/core';

class SnackbarLoading extends Component {
	state = { open: false }
	constructor(){
		super();

		this.handleClick = this.handleClick.bind(this);
		this.handleClose = this.handleClose.bind(this);
	}
	componentWillMount(){
		this.props.handleClick(this.handleClick)
		this.props.handleClose(this.handleClose)
	}
	handleClick = () => {
		this.setState({ open: true })
	}
	handleClose = () => {
		this.setState({ open: false })
	}
	render() {
		return(
			<SnackbarMaterial
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'left',
				}}
				open={this.state.open}
				// onClose={this.handleClose}
				ContentProps={{
					'aria-describedby': 'message-id',
				}}
				message={<span id="message-id">Cargando...</span>
				}
				action={
					<CircularProgress color="inherit"/>
				}
			/>
		)
	}
}

export default SnackbarLoading;