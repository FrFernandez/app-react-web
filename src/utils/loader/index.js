import React from 'react';
import { CircularProgress } from '@material-ui/core';
import './index.css';

const Loader = () => (
	<div className='Loader'>
		<div className="center">
			<CircularProgress size={100}/>
		</div>
	</div>
);

export default Loader;